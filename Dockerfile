ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_python:3.8.2-headless

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

RUN mkdir -p /opt/bin/ && echo "#!/bin/bash" > /opt/bin/module && chmod a+x /opt/bin/module

ENV PATH_TO_PHOENIX_REPO /opt/phoenix/
COPY samStats2json.py /opt/phoenix/required_scripts/
